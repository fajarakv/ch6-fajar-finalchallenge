const express = require("express");
const app = express();
const { user_game, user_game_biodata, user_game_history } = require("./models");

app.use(express.json());
app.use(express.urlencoded({extended: false}));

app.post("/usercreate", async (req,res) => {
  const {username,password,first_name,last_name,email,phone,birthdate,city,time_played,score,status} = req.body;
  const found = await user_game.findOne({where: {username:username}});

  if (found) {
    res.json({ message: "Username existed, try different username"})
  } else {
    user_game.create({
      username,
      password,
    }).then((user_game) => {
      user_game_history.create({
        time_played,
        score,
        status,
        user_id: user_game.id
      });
  
      user_game_biodata.create({
        first_name,
        last_name,
        email,
        phone,
        birthdate,
        city,
        user_id: user_game.id,
      });
  
      res.json({ message: "Create Success", data: user_game})
    });
  };
});

app.post("/historycreate", async (req,res) => {
  const {time_played,score,status,user_id} = req.body;
  const found = await user_game.findOne({where: {id:user_id}});
  if (found) {
    user_game_history.create({
      time_played,
      score,
      status,
      user_id
    }).then((user_game_history) => {
      res.json({ message: "Create Success", data: user_game_history});
    });
  } else {
    res.status(404).json({
      message: "Id not existed"
    });
  };
});

app.get("/userAll", (req,res) => {
  user_game.findAll({
    include: [  
      {model: user_game_biodata, as: "biodata"}, 
      {model: user_game_history, as: "history"}
    ]
  }).then(
    (user_game) => {
    res.json({ message: "Fetch All Success", data: user_game});
  });
});

app.get("/user/:id", async (req,res) => {
  const {id} = req.params;
  const found = await user_game.findOne({where: {id:id}});

  if (found) {
    user_game.findOne({
      where: { id: id },
      include: [
        {model: user_game_biodata, as: "biodata"},
        {model: user_game_history, as: "history"}
      ]
    }).then(
      (user_game) => {
      res.json({ message: "Fetch Data Success", data: user_game});
      });
  } else {
    res.status(404).json({
      message: "Id not existed"
    });
  };
});

app.delete("/delete/:id", async (req,res) => {
  const {id} = req.params;
  const found = await user_game.findOne({where: {id:id}});

  if (found) {
    user_game.destroy({
      where: {id:id},
    }).then((user_game) => {
      user_game_biodata.destroy({
        where: {user_id: id}
      });
  
      user_game_history.destroy({
        where: {user_id: id}
      });
  
      res.json({message: "Delete Success"});
    });
  } else {
    res.status(404).json({
      message: "Id not existed"
    });
  };
});

app.put("/update/:id", async (req,res) => {
  const {id} = req.params;
  const {username,password,first_name,last_name,email,phone,birthdate,city} = req.body;
  const found = await user_game.findOne({where: {id:id}});

  if (found) {
    user_game.update({
    username,
    password,
  }, {
    where: {id:id}
  })

    user_game_biodata.update({
      first_name,
      last_name,
      email,
      phone,
      birthdate,
      city,
    }, {
      where: {id:id}
    });

    res.json({ message: "Update Success"})
  } else {
    res.status(404).json({
      message: "Id not existed"
    });
  };
});

app.listen(8000, () => console.log(`Listening at http://localhost:${8000}`));